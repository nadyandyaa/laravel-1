<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form</title>
</head> 
<body>
<h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
    @csrf
        <label>First name:</label> <br>
        <input type="text" name="awal"> <br><br>
        <label>Last name:</label><br>
        <input type="text" name="akhir"> <br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender" >Male<br>
        <input type="radio" name="gender" >Female<br>
        <input type="radio" name="gender" >Other<br><br>
        <label>Nationality :</label><br>
        <select name="Nationality">
            <option value="i">Indonesian</option>
            <option value="sg">Singaporean</option>
            <option value="m">Malaysian</option>
            <option value="a">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type ="checkbox">Bahasa Indonesia<br>
        <input type ="checkbox">English<br>
        <input type ="checkbox">Other<br><br>
        <label>Bio:</label><br>
        <textarea name = "bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign up">
    </form>
</body>
</html>