<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@awal');

Route::get('/awal', 'HomeController@awal');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');


    // Route::get('/', function () {
    //     return view('awal');
    // });

    // Route::get('/form/{name}', function($name){
    //     return view ('form' , ["name" => $name]);
    // });

    // Route::get('/welcome/{name}', function ($name) {
    //     return "welcome $name";
    // });

    // Route::get('/form', 'RegisterController@form'); 

    // Route::get('/sapa', 'RegisterController@sapa');
    // Route::post('/sapa','RegisterController@sapa_post');